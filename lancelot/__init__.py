from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

lancelot = Flask(__name__)
lancelot.config.from_object('config')
db = SQLAlchemy(lancelot)
from lancelot import views,models