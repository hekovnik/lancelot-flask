from lancelot import lancelot, db, models
import requests
from flask import jsonify, abort, make_response, request

@lancelot.route('/')
def index():
    return "Hello, World!"

@lancelot.route('/subscribers', methods = ['GET'])
def get_subscriptions():
	""" get all subscriptions """
	return jsonify({ 'subscribers': [i.serialize for i in models.Subscriber.query.all()] })

@lancelot.route('/subscribers', methods = ['POST'])
def add_subscription():
	""" subscribe a module """
	if not request.json: # or if not validate_subscription(request.json):
		abort(400)
	subscription = models.Subscriber(name = request.json['name'],
									url = request.json['url'],
									tag = request.json['tag'])
	db.session.add(subscription)
	db.session.commit()
	return jsonify({'subscriber' : subscription.serialize }),201

@lancelot.route('/subscribers/<int:subscription_id>', methods = ['DELETE'])
def delete_subscription(subscription_id):
	""" delete a subscription """
	subscription = models.Subscriber.query.get(subscription_id)
	if subscription == None:
		abort(404)
	db.session.delete(subscription)
	return jsonify({'result' : True })

@lancelot.route('/subscribers/<int:subscription_id>', methods = ['GET'])
def get_subscription(subscription_id):
	""" get subscription with id """
	subscription = models.Subscriber.query.get(subscription_id)
	if subscription == None:
		abort(404)
	return jsonify({'subscriber' : subscription.serialize })

@lancelot.route('/new_note', methods = ['POST'])
def analyze_note():
	""" get new note and route it """
	if not request.json: # TODO - validate note
		abort(400)
	
	note_tags = request.json['tags']
	subscribers = models.Subscriber.query.all()
	
	for subscriber in subscribers:
		if subscriber.tag in note_tags:
			if not post_to_module(subscriber,request.json):
				# save note to db for later
				
	return make_response(jsonify( {'success': 'True'} ),201)

@lancelot.errorhandler(404)
def not_found(error):
	""" json error handling """
	return make_response(jsonify( {'error': 'Not found'} ),404)

def validate_subscription(subscription):
	""" validate subscription, TODO validate URL """
	pass

def post_to_module(module,content):
	""" This functions relays notes to designated module """
	r = requests.post(url=module.url, data=content, headers={'content-type' : 'application/json'})
	return r.status_code == 200