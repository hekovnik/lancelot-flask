from lancelot import db

class Subscriber(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.String(100))
	url = db.Column(db.String(60))
	tag = db.Column(db.String(30))
	
	@property
	def serialize(self):
	   """ models serializer """
	   return {
		   'id' : self.id,
		   'name' : self.name,
		   'url' : self.url,
		   'tag' : self.tag
		}