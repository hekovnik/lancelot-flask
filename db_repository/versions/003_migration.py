from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
subscriber = Table('subscriber', pre_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String),
    Column('ip', Integer),
    Column('port', Integer),
    Column('tag', String),
)

subscriber = Table('subscriber', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=100)),
    Column('url', String(length=60)),
    Column('tag', String(length=30)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['subscriber'].columns['ip'].drop()
    pre_meta.tables['subscriber'].columns['port'].drop()
    post_meta.tables['subscriber'].columns['url'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['subscriber'].columns['ip'].create()
    pre_meta.tables['subscriber'].columns['port'].create()
    post_meta.tables['subscriber'].columns['url'].drop()
